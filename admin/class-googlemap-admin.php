<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://profiles.wordpress.org/smsawoncse/
 * @since      1.0.0
 *
 * @package    Googlemap
 * @subpackage Googlemap/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Googlemap
 * @subpackage Googlemap/admin
 * @author     Sahed Sawon <plugin.sahed.sawon@gmail.com>
 */
class Googlemap_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Googlemap_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Googlemap_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/googlemap-admin.css', array(), $this->version, 'all' );
		wp_enqueue_script( 'gogoleMapJsAPI', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyC3H4XFCRNfOmZpg5mUu3RQJv7j5Dbp8wM&libraries=places', array( 'jquery' ), $this->version, false );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Googlemap_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Googlemap_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/googlemap-admin.js', array( 'jquery' ), $this->version, false );

	}

	public function googlemap_widget_register()
	{
		register_widget('googlemapwidget');
	}

}

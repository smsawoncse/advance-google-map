<?php

/**
 * Fired during plugin activation
 *
 * @link       https://profiles.wordpress.org/smsawoncse/
 * @since      1.0.0
 *
 * @package    Googlemap
 * @subpackage Googlemap/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Googlemap
 * @subpackage Googlemap/includes
 * @author     Sahed Sawon <plugin.sahed.sawon@gmail.com>
 */
class Googlemap_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}

<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://profiles.wordpress.org/smsawoncse/
 * @since      1.0.0
 *
 * @package    Googlemap
 * @subpackage Googlemap/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Googlemap
 * @subpackage Googlemap/includes
 * @author     Sahed Sawon <plugin.sahed.sawon@gmail.com>
 */
class Googlemap_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}

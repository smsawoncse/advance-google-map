<?php

class googlemapwidget  extends WP_Widget{
	function __construct() {
		// Instantiate the parent object
		parent::__construct( 
			'advance_google_map', //Base Id	
			__('Advance Google Map'), // Name of the widget
			 array(	'description' => __('Advance google map with lot of map features.') ) // Args		
			 );
	}

	function widget( $args, $instance ) {
		// Widget output
		echo $args['before_widget'];

		if ( !empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
		} else {
			echo $args['before_title'] . apply_filters( 'widget_title', 'Google Map' ) . $args['after_title'];
		}

		?>
		<div id="map" style="height:<?php echo !empty($instance['mapHeight'])?$instance['mapHeight']:"275"; ?>px;width: 100%"></div>
	    <script>
	    	function initMap(lat,long) {
	      	var mycenter = {lat: lat, lng: long};
	        var map = new google.maps.Map(document.getElementById('map'), {
	          center: mycenter,
	          zoom: <?php echo $instance['zoom']; ?>,
	          mapTypeId: '<?php echo $instance['mapType']; ?>'
	        });
	        var iconBase = 'https://maps.google.com/mapfiles/kml/shapes/';
	        var marker = new google.maps.Marker({
	        	position: mycenter ,
	        	icon :  '<?php echo wp_make_link_relative(plugin_dir_url(dirname(__FILE__)).'public/images/placeholder.png');?>'
	        });
			marker.setMap(map);
	        //map.setTilt(45);
	      }

	    	var geocoder = new google.maps.Geocoder();
			var address = '<?php echo $instance['map_location'];?>';
			 geocoder.geocode( { 'address': address}, function(results, status) {
			  if (status == google.maps.GeocoderStatus.OK)
			  {
			  	var lat = results[0].geometry.location.lat();
			  	var lng =results[0].geometry.location.lng();
			  	initMap(lat,lng);
			  }else{
			  	alert('Google geocode says '+status);
			  }
			});
	      
	      //initMap();
	    </script>
		<?php
		echo $args['after_widget'];
	}
	function update( $new_instance, $old_instance ) {
		// Save widget options
		$instance          = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['map_location'] = ( ! empty( $new_instance['map_location'] ) ) ? strip_tags( $new_instance['map_location'] ) : '';
		$instance['mapType'] = ( ! empty( $new_instance['mapType'] ) ) ? strip_tags( $new_instance['mapType'] ) : '';
		$instance['zoom'] = ( ! empty( $new_instance['zoom'] ) ) ? strip_tags( $new_instance['zoom'] ) : '';
		$instance['mapHeight'] = ( ! empty( $new_instance['mapHeight'] ) ) ? strip_tags( $new_instance['mapHeight'] ) : '';
		
		return $instance;
	}

	function form( $instance ) {
		// Output admin widget options form
		$title = !empty($instance['title']) ? $instance['title'] : __( 'Google Map', 'text_domain' );
		$map_location = !empty($instance['map_location']) ? $instance['map_location'] : __( 'Bangladesh', 'text_domain' );
		$mapType = !empty($instance['mapType']) ? $instance['mapType'] : __( 'roadmap', 'text_domain' );
		$mapHeight = !empty($instance['mapHeight'])? $instance['mapHeight']:'';
		$zoom = !empty($instance['zoom']) ? $instance['zoom'] : __( 8, 'text_domain' );
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>"
			name="<?php echo $this->get_field_name( 'title' ); ?>" type="text"
			value="<?php echo esc_attr( $title ); ?>">
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'map_location' ); ?>"><?php _e( 'Map Location:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'map_location' ); ?>"
			name="<?php echo $this->get_field_name( 'map_location' ); ?>" type="text"
			value="<?php echo esc_attr( $map_location ); ?>">
			<script type="text/javascript">
				 function  placeAutocomplete(input){
					var input = /** @type {!HTMLInputElement} */(
				            document.getElementById(input));
					var options = {
						  types: ['geocode'] 
						};
					var autocomplete = new google.maps.places.Autocomplete(input,options);
				}
				var placeinputone = '<?php echo $this->get_field_id( 'map_location' ); ?>';
				document.getElementById(placeinputone).onkeyup = function() {placeAutocomplete(placeinputone)};

			</script>
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'mapType' ); ?>"><?php _e( 'Map Type:' ); ?></label>
			<select id="<?php echo $this->get_field_id( 'mapType' ); ?>" name="<?php echo $this->get_field_name( 'mapType' ); ?>" style="width: 100%">
				<option <?php selected( $mapType, 'satellite' , true ); ?>  value="satellite">Satellite</option>
				<option <?php selected( $mapType, 'hybrid' , true ); ?>  value="hybrid">Hybrid</option>
				<option <?php selected( $mapType, 'terrain' , true ); ?>  value="terrain">Terrain</option>
				<option <?php selected( $mapType, 'roadmap' , true ); ?> value="roadmap">Roadmap</option>
			</select>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'mapHeight' ); ?>"><?php _e( 'Map Height (in pixel):' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'mapHeight' ); ?>"
			name="<?php echo $this->get_field_name( 'mapHeight' ); ?>" type="number" min="8"	value="<?php echo esc_attr( $mapHeight ); ?>">
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'zoom' ); ?>"><?php _e( 'Map Zoom:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'zoom' ); ?>"
			name="<?php echo $this->get_field_name( 'zoom' ); ?>" type="number" min="8" max="25"
			value="<?php echo esc_attr( $zoom ); ?>">
		</p>

		<?php
	}
}

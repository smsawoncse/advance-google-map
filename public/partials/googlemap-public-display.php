<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       https://profiles.wordpress.org/smsawoncse/
 * @since      1.0.0
 *
 * @package    Googlemap
 * @subpackage Googlemap/public/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
